<?php

namespace App\Providers;

use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;

use App\Http\Controllers\Admin\RegisteredUserController;
use App\Http\Controllers\Admin\AuthenticatedSessionController;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Support\Facades\Auth;
use App\Actions\Fortify\Admin\AttemptToAuthenticate;
use App\Actions\Fortify\Admin\RedirectIfTwoFactorAuthenticatable;

use App\Models\User;
use App\Models\Customer;

class FortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->when([AuthenticatedSessionController::class,AttemptToAuthenticate::class,RedirectIfTwoFactorAuthenticatable::class,RegisteredUserController::class])
            ->needs(StatefulGuard::class)
            ->give(function(){
                return Auth::guard('admin');
            });
         
        //  if(request()->is('admin/*'))
        //  {
        //     Config::set('fortify.guard','admin');
        //     Config::set('fortify.home','/admin/home');
        //     dd(request()->is('admin/*'),Config::get('fortify.guard'),Config::get('fortify.home'));
           
        //  }
        
         
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        Fortify::createUsersUsing(CreateNewUser::class);
        Fortify::updateUserProfileInformationUsing(UpdateUserProfileInformation::class);
        Fortify::updateUserPasswordsUsing(UpdateUserPassword::class);
        Fortify::resetUserPasswordsUsing(ResetUserPassword::class);

        RateLimiter::for('login', function (Request $request) {
            return Limit::perMinute(100)->by($request->email.$request->ip());
        });

        RateLimiter::for('two-factor', function (Request $request) {
            return Limit::perMinute(100)->by($request->session()->get('login.id'));
        });

        Fortify::registerView(function () {
            return view('auth.register'); 
        });
        Fortify::loginView(function () {
            return view('auth.login'); 
        });

        Fortify::requestPasswordResetLinkView(function () {
            return view('auth.forgot'); 
        });

        Fortify::resetPasswordView(function () {
            return view('auth.forgot-form'); 
        });

        Fortify::verifyEmailView(function () {
            return view('auth.email-verify'); 
        });
        

    }
}
