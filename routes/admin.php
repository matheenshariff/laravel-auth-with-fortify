<?php 


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AuthenticatedSessionController;
use App\Http\Controllers\Admin\RegisteredUserController;



Route::group(['as' => 'admin.'],function(){

    Route::group(['middleware' => 'guest:admin'],function(){
        Route::get('register',[RegisteredUserController::class,'create'])->name('register');
        Route::post('register',[RegisteredUserController::class,'store'])->name('register');

        Route::get('login',[AuthenticatedSessionController::class,'create'])->name('login');
        Route::post('login',[AuthenticatedSessionController::class,'store'])->name('login');
    });

    Route::group(['middleware' => 'auth:admin'],function(){
        Route::post('logout',[AuthenticatedSessionController::class,'destroy'])->name('logout');
        Route::get('home',function(){
            return view('auth.admin.home');
        })->name('home');
    });
   

});

